package com.example.diceroller

import android.os.Bundle
import android.view.Gravity
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity

class MainActivity : AppCompatActivity() {
    lateinit var rollButton: Button
    lateinit var resultImageView: ImageView
    lateinit var resultImageView2: ImageView
    lateinit var resetButton: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //Values...
        val text = "Roll the dice!"
        val duration = Toast.LENGTH_LONG
        val toast = Toast.makeText(applicationContext, text, duration)
        toast.show()

        val resetText = "Dice gone!"
        val resetDuration = Toast.LENGTH_SHORT

        //Array de imagenes
        val images = arrayOf(R.drawable.dice_1, R.drawable.dice_2, R.drawable.dice_3, R.drawable.dice_4,
            R.drawable.dice_5, R.drawable.dice_6)

        //Ids
        rollButton = findViewById(R.id.roll_buttom)
        resultImageView = findViewById(R.id.dice1)
        resultImageView2 = findViewById(R.id.dice2)
        resetButton = findViewById(R.id.reset_buttom)

        //Roll Button
        rollButton.setOnClickListener {
            //Random values
            val random = (0..5).random()
            val random2 = (0..5).random()

            //Jackpot Toast
            val textJackpot = "JACKPOT!"
            val jpDur = Toast.LENGTH_LONG

            //Funciones...
            resultImageView.visibility = View.VISIBLE
            resultImageView2.visibility = View.VISIBLE

            resultImageView.setImageResource(images[random])
            resultImageView2.setImageResource(images[random2])

            if(random == 5 && random2 == 5){
                val jpToast = Toast.makeText(baseContext, textJackpot, jpDur)
                jpToast.setGravity(Gravity.TOP, 0, 0)
                jpToast.show()
            }
        }

        //Reset Button
        resetButton.setOnClickListener{
            resultImageView.visibility = View.INVISIBLE
            resultImageView2.visibility = View.INVISIBLE
            val toastReset = Toast.makeText(baseContext, resetText, resetDuration)
            toastReset.show()
        }

        //Unicamente a un dado...
        resultImageView.setOnClickListener{
            val random = (0 until 6).random()
            resultImageView.setImageResource(images[random])

        }
        resultImageView2.setOnClickListener{
            val random2 = (0 until 6).random()
            resultImageView2.setImageResource(images[random2])
        }
    }
}